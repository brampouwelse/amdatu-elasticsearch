/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.elasticsearch.node;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.elasticsearch.ElasticsearchTimeoutException;
import org.elasticsearch.ExceptionsHelper;
import org.elasticsearch.common.collect.Tuple;
import org.elasticsearch.common.http.client.HttpDownloadHelper;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.ImmutableSettings.Builder;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.env.Environment;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.internal.InternalSettingsPreparer;
import org.elasticsearch.plugins.PluginManager;
import org.elasticsearch.plugins.PluginManager.OutputMode;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

public class ElasticsearchClusterNodeFactory implements ManagedServiceFactory {

	public static final String PID = "org.amdatu.elasticsearch.node.factory";

	public static final TimeValue DEFAULT_PLUGIN_INSTALL_TIMEOUT = TimeValue.timeValueSeconds(120l);

	private volatile DependencyManager m_dependencyManager;

	private volatile LogService m_logService;

	private final Map<String, Component> components = new HashMap<>();

	@Override
	public String getName() {
		return PID;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void updated(String pid, Dictionary properties) throws ConfigurationException {
		deleted(pid);

		Dictionary<String, Object> p = new Hashtable<>();
		Enumeration<String> keys = properties.keys();
		while (keys.hasMoreElements()) {
			String nextElement = keys.nextElement();
			p.put(nextElement, properties.get(nextElement));

		}

		if (p.get("path.plugins") == null) {
			Bundle esBundle = FrameworkUtil.getBundle(ImmutableSettings.class);
			File pluginsDir = esBundle.getDataFile("es-plugins-" + pid);
			if (!pluginsDir.exists()) {
				if (!pluginsDir.mkdir()) {
					m_logService.log(LogService.LOG_WARNING, "Unable to create plugin dir");
				}
			}
			p.put("path.plugins", pluginsDir.getAbsolutePath());
		}

		Settings settings = createSettings(p);

		List<PluginHandle> plugins = parsePluginsToInstall(p);

		installPlugins(plugins, settings);

		ElasticsearchClusterNodeImpl elasticsearchClusterNodeImpl = new ElasticsearchClusterNodeImpl(settings);

		Properties props = new Properties();
		props.put("clusterName", elasticsearchClusterNodeImpl.getClusterName());

		Component component = m_dependencyManager.createComponent().setInterface(Node.class.getName(), props)
				.setImplementation(elasticsearchClusterNodeImpl)
				.setCallbacks("dmInit", "dmStart", "dmStop", "dmDestroy")
				.add(m_dependencyManager.createServiceDependency().setService(LogService.class).setRequired(false));

		components.put(pid, component);
		m_dependencyManager.add(component);

	}

	private List<PluginHandle> parsePluginsToInstall(Dictionary<String, Object> p) throws ConfigurationException {

		String pluginsProperty = (String) p.get("plugin.mandatory");
		if (pluginsProperty == null || pluginsProperty.trim().isEmpty()) {
			return Collections.emptyList();
		}

		List<PluginHandle> pluginHandles = new ArrayList<>();

		String[] plugins = pluginsProperty.split(",");
		for (String plugin : plugins) {
			String handlePropertyKey = "org.amdatu.elasticsearch.plugin." + plugin.trim() + ".handle";
			String handle = getRequiredStringProperty(handlePropertyKey, p);

			pluginHandles.add(PluginHandle.parse(handle));
		}

		return pluginHandles;
	}

	private void installPlugins(List<PluginHandle> plugins, Settings settings) {
		try {

			Tuple<Settings, Environment> envSettings = InternalSettingsPreparer.prepareSettings(settings, false);

			Bundle esBundle = FrameworkUtil.getBundle(ImmutableSettings.class);
			File dataFile = esBundle.getDataFile("plugin-cache");

			HttpDownloadHelper downloadHelper = new HttpDownloadHelper();

			for (PluginHandle pluginHandle : plugins) {
				File file = new File(dataFile, pluginHandle.user + "/" + pluginHandle.name);
				File pluginFile = new File(file, pluginHandle.version + ".zip");

				if (pluginFile.exists() && (pluginHandle.version == null || "latest".equals(pluginHandle.version))) {
					m_logService.log(LogService.LOG_DEBUG, "Removing cached version of latest plugin "
							+ pluginHandle.user + "/" + pluginHandle.name);
					pluginFile.delete();
				}

				if (!pluginFile.exists()) {

					if (!file.exists()) {
						file.mkdirs();
					}

					boolean downloaded = false;
					for (URL url : pluginHandle.urls()) {

						m_logService.log(LogService.LOG_DEBUG, "Trying " + url.toExternalForm() + "...");
						try {
							downloadHelper.download(url, pluginFile, null, TimeValue.timeValueMinutes(1L));
							downloaded = true;
							break;
						} catch (ElasticsearchTimeoutException e) {
							throw e;
						} catch (Exception e) {
							m_logService.log(LogService.LOG_ERROR, "Failed: " + ExceptionsHelper.detailedMessage(e));
						}
					}

					if (!downloaded) {
						throw new RuntimeException("Failed to download plugin " + pluginHandle.user + "/"
								+ pluginHandle.name + "/" + pluginHandle.version);
					}

				}

				String url = pluginFile.toURI().toURL().toExternalForm();
				PluginManager pluginManager = new PluginManager(envSettings.v2(), url, OutputMode.VERBOSE,
						DEFAULT_PLUGIN_INSTALL_TIMEOUT);
				pluginManager.downloadAndExtract(pluginHandle.user + "/" + pluginHandle.name + "/"
						+ pluginHandle.version);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deleted(String pid) {
		Component component = components.remove(pid);
		if (component != null) {
			m_dependencyManager.remove(component);
		}
	}

	@SuppressWarnings("unused" /* dependency manager callback */)
	private void stop() {
		for (Component component : components.values()) {
			m_dependencyManager.remove(component);
		}
	}

	private Settings createSettings(Dictionary<String, ?> properties) throws ConfigurationException {
		Builder builder = ImmutableSettings.builder().classLoader(ImmutableSettings.class.getClassLoader());

		/*
		 * Cluster
		 */
		builder.put("cluster.name", getRequiredStringProperty("cluster.name", properties));

		/*
		 * Node
		 */
		builder.put("node.name", getRequiredStringProperty("node.name", properties));

		String nodeMaster = getOptionalBooleanProperty("node.master", properties);
		if (nodeMaster != null) {
			builder.put("node.master", nodeMaster);
		}

		String nodeData = getOptionalBooleanProperty("node.data", properties);
		if (nodeData != null) {
			builder.put("node.data", nodeData);
		}

		String nodeRack = (String) properties.get("node.rack");
		if (nodeRack != null) {
			builder.put("node.rack", nodeRack);
		}

		String nodeMaxLocalStorageNodes = (String) properties.get("node.max_local_storage_nodes");
		if (nodeMaxLocalStorageNodes != null) {
			try {
				builder.put("node.max_local_storage_nodes", Integer.parseInt(nodeMaxLocalStorageNodes));
			} catch (NumberFormatException e) {
				throw new ConfigurationException("node.max_local_storage_nodes", "Invalid number", e);
			}
		}

		/*
		 * Paths
		 */
		String pathLogs = (String) properties.get("path.logs");
		if (pathLogs != null) {
			builder.put("path.logs", pathLogs);
		}

		String pathData = (String) properties.get("path.data");
		if (pathData != null) {
			builder.put("path.data", pathData);
		}

		String pathWork = (String) properties.get("path.work");
		if (pathWork != null) {
			builder.put("path.work", pathWork);
		}

		builder.put("path.plugins", properties.get("path.plugins"));

		builder.classLoader(new URLClassLoader(new URL[] {}, ImmutableSettings.class.getClassLoader()));

		return builder.build();
	}

	private String getRequiredStringProperty(String key, Dictionary<String, ?> properties)
			throws ConfigurationException {
		String clusterNameProperty = (String) properties.get(key);
		if (!(clusterNameProperty instanceof String)) {
			throw new ConfigurationException(key, "property is required");
		}
		return clusterNameProperty;
	}

	private String getOptionalBooleanProperty(String key, Dictionary<String, ?> properties)
			throws ConfigurationException {
		String value = (String) properties.get(key);
		if (!(value == null || "true".equals(value) || "false".equals(value))) {
			throw new ConfigurationException(key, "Invalid value for property " + key + " should be true or false was "
					+ value);
		}
		return value;
	}

}
